import {Plants} from "../plants/plants";

export interface Annonces{
  id: number,
  title : string,
  status : string,
  description : string,
  startDate : string, 
  endDate : string,
  publishedDate : string
}


