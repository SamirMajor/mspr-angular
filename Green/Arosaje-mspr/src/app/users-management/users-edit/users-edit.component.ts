import {Component, OnInit} from '@angular/core';
import {UsersDetailsComponent} from "../users-details/users-details.component";
import {UsersService} from "../../services/users/users.service";
import {ActivatedRoute, Router} from "@angular/router";
import {FormBuilder} from "@angular/forms";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-users-edit',
  templateUrl: '../users-details/users-details.component.html',
  styleUrls: ['../users-details/users-details.component.css']
})
export class UsersEditComponent extends UsersDetailsComponent implements OnInit{

  constructor(
    private  usersService: UsersService,
    private route : ActivatedRoute,
    fb : FormBuilder,
    router : Router,
    private snackBar : MatSnackBar) {
    super(false,fb,router);
  }

  ngOnInit(): void {
    super.onInit();
    this.getUser();
  }


  validateForm(): void {
    console.log('LdapEditComponent - validateForm');
    this.processValidateRunning = true;
    this.usersService.updateUser(this.getUserFromFormControl()).subscribe(
      data => {
        this.processValidateRunning = false;
        this.errorMessage = '';
        this.snackBar.open('Utilisateur modifié !', 'X');
        this.router.navigate(['/users/list']);
      },
      error => {
        this.processValidateRunning = false;
        this.errorMessage = 'Une erreur est survenue dans la modification !';
        this.snackBar.open('Utilisateur non modifié !', 'X');
      }
    );
  }

  private getUser():void {
    const login = this.route.snapshot.paramMap.get('id');

    this.processLoadRunning = true;

    this.usersService.getUser(Number(login)).subscribe(
      user => {
        this.user = user;
        this.copyUserToFormControl();
        this.processLoadRunning = false;
      },
      error => {
        this.processLoadRunning = false;
        this.errorMessage = 'L\'utilisateur n\'existe pas !';
        this.snackBar.open('Utilisateur non trouvé !', 'X');
      }
    );
  }
  deleteUser():void {
    const login = this.route.snapshot.paramMap.get('id');

    this.processLoadRunning = true;

    this.usersService.deleteUser(Number(login)).subscribe(
      user => {
        this.user = user;
        this.processLoadRunning = false;
        this.router.navigate(['/users/list']);
      },
      error => {
        this.processLoadRunning = false;
        this.errorMessage = 'L\'utilisateur n\'existe pas !';
        this.snackBar.open('Utilisateur non trouvé !', 'X');
      }
    );

  }
}
