import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BrowserModule} from "@angular/platform-browser";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {AppMaterialModule} from "../app-material.module";
import {HttpClientModule} from "@angular/common/http";
import {AppRoutingModule} from "../app-routing.module";
import {UsersListComponent} from "./users-list/users-list.component";
import {UsersAddComponent} from "./users-add/users-add.component";
import {UsersEditComponent} from "./users-edit/users-edit.component";
import {UsersManagementRoutingModule} from "./users-management-routing.module";




@NgModule({
  declarations: [
    UsersListComponent,
    UsersAddComponent,
    UsersEditComponent,
  ],
  imports: [
  CommonModule,
  BrowserModule,
  FormsModule,
  ReactiveFormsModule,
  AppMaterialModule,
  UsersManagementRoutingModule,
  HttpClientModule,
  AppRoutingModule
  ]
})
export class UsersManagementModule { }
