import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from "@angular/material/paginator";
import {UsersService} from "../../services/users/users.service";
import {Router} from "@angular/router";
import {MatSlideToggleChange} from "@angular/material/slide-toggle";

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit, AfterViewInit{

  displayedColumns: string[] = ['firstName','lastName','mail', 'role'];
  dataSource :any[];
  unactiveSelected=false;

  @ViewChild(MatPaginator,{static:true}) paginator : MatPaginator;

  constructor(private usersService: UsersService, private router: Router) { }

  ngOnInit(): void {
    //console.log('Value on ngOninit():');
    // this.dataSource.paginator = this.paginator;
    // this.dataSource.filterPredicate = (data: UserLdap, filter: string) => this.filterPredicate(data, filter);
    this.getUsers();
    console.log('mat paginator : ' );

  }

  ngAfterViewInit(): void {


  }


  filterPredicate(data,filter):boolean{
    return !filter || data.nomComplet.toLowerCase().startsWith(filter);
  }

  applyFilter($event: KeyboardEvent):void{
    const filterValue=($event.target as HTMLInputElement).value;
    //  this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  private getUsers():void{
    console.log('ldap comp');
    this.usersService.getUsers().subscribe(
      data => {
        this.dataSource = data;
        console.log(data );

      }
    );
  }

  unactiveChanged($event:MatSlideToggleChange):void{
    this.unactiveSelected=$event.checked;
    this.getUsers();
  }

  edit(id : number){
    this.router.navigate(['/users', id]).then( (e) => {
      if (! e) {
        console.log('navigation has failed ');

      }
    })
  }

  addUser(){
    this.router.navigate(['/users/add']).then( (e) => {
      if (! e){
        console.log('Navigation has failed!');
      }
    });
  }
}
