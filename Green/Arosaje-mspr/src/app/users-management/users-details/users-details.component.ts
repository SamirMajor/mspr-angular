import {Users} from "../../models/users/users";
import {FormBuilder} from "@angular/forms";
import {Router} from "@angular/router";


export abstract class UsersDetailsComponent{

  user: Users;
  processLoadRunning = false;
  processValidateRunning = false;
  errorMessage = '';

  userForm = this.fb.group({
    login: [''],
    nom: [''],
    prenom: [''],
    role:[''],
    mail: {value: '', disabled: true},

  });

  protected constructor(
    public addForm: boolean,
    /* protected route: ActivatedRoute, */
    private fb: FormBuilder,
    public router: Router,
  ) {

  }

  protected onInit(): void {

  }


  private formGetValue(name: string): any {
    return this.userForm.get(name)!.value;
  }

  goToLdap(): void {
    this.router.navigate(['/users']);
  }


  abstract validateForm(): void;

  abstract deleteUser(): void;

  onSubmitForm(): void {
    this.validateForm();
  }

  delete(): void {
    this.deleteUser();
  }

  updateLogin(): void {
    if (this.addForm) {
      this.userForm.get('login')!.setValue((this.formGetValue('prenom')
        + '.' + this.formGetValue('nom')).toLowerCase());
      this.updateMail();
    }
  }

  updateMail(): void {
    if (this.addForm) {
      this.userForm.get('mail')!.setValue(this.formGetValue('login').toLowerCase() + '@epsi.lan')
    }
  }

  isFormValid(): boolean {
    return this.userForm.valid

      && (!this.addForm);
  }

  public copyUserToFormControl(): void {
    this.userForm.get('login')!.setValue(this.user.login);
    this.userForm.get('nom')!.setValue(this.user.name);
    this.userForm.get('prenom')!.setValue(this.user.prenom);
    this.userForm.get('mail')!.setValue(this.user.mail);
    this.userForm.get('role')!.setValue(this.user.role);


  }

  protected getUserFromFormControl(): any {
    if (this.addForm) {
      return  {
        login: this.userForm.get('login')!.value,
        name: this.userForm.get('nom')!.value,
        prenom: this.userForm.get('prenom')!.value,
        mail: this.userForm.get('mail')!.value,
        role: this.userForm.get('role')!.value,
      };
    }

  }
}
