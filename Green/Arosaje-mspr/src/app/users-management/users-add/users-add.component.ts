import {Component, OnInit} from '@angular/core';
import {UsersDetailsComponent} from "../users-details/users-details.component";
import {UsersService} from "../../services/users/users.service";
import {FormBuilder} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-users-edit',
  templateUrl: '../users-details/users-details.component.html',
  styleUrls: ['../users-details/users-details.component.css']
})
export class UsersAddComponent extends UsersDetailsComponent implements OnInit{


  constructor(private usersService : UsersService,
              fb: FormBuilder,
              private route : ActivatedRoute,
              router: Router,
              private snackBar : MatSnackBar) {
    super(true,fb,router);
  }

  ngOnInit(): void {
    super.onInit();
  }

  validateForm() : void {
    console.log('LdapAddComponent - validateForm');
    this.processValidateRunning = true;
    this.usersService.addUser(this.getUserFromFormControl()).subscribe(
      data => {
        this.processValidateRunning = false;
        this.errorMessage = '';
        this.snackBar.open('Utilisateur ajouté !', 'X');
        this.router.navigate(['/users/list']);
      },
      error => {
        this.processLoadRunning = false;
        this.errorMessage = 'L\'utilisateur n\'a pas pu etre ajouté !';
        this.snackBar.open('Erreur dans l\'ajout de l\'utilisateur!', 'X');
      }
    );
  }


  deleteUser():void {
    const login = this.route.snapshot.paramMap.get('id');

    this.processLoadRunning = true;

    this.usersService.deleteUser(Number(login));

  }
}
