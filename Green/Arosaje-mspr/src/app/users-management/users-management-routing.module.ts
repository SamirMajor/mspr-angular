import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {UsersListComponent} from "./users-list/users-list.component";
import {UsersAddComponent} from "./users-add/users-add.component";
import {UsersEditComponent} from "./users-edit/users-edit.component";





const adminRoutes: Routes = [
  {path :'users',component : UsersListComponent},
  {path :'users/edit', component : UsersEditComponent},
  {path :'users/:id', component : UsersAddComponent}
];

@NgModule({
  imports: [RouterModule.forChild(adminRoutes)],
  exports: [RouterModule]
})

export class UsersManagementRoutingModule { }
