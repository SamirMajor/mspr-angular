import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashbordComponent } from './dashbord/dashbord.component';
import {HttpClientModule} from "@angular/common/http";
import {AppMaterialModule} from "./app-material.module";
import {UsersService} from "./services/users/users.service";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {MatCardModule} from "@angular/material/card";
import {UsersManagementModule} from "./users-management/users-management.module";
import { AnnoncesComponent } from './annonces/annonces.component';





@NgModule({
  declarations: [
    AppComponent,
    DashbordComponent,
    AnnoncesComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    AppMaterialModule,
    UsersManagementModule,
    AppRoutingModule,
    HttpClientModule,
    MatCardModule,
  ],
  providers: [UsersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
