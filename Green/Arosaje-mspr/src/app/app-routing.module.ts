import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {NavbarComponent} from "./navbar/navbar.component";
import {DashbordComponent} from "./dashbord/dashbord.component";
import {MatSidenavModule} from "@angular/material/sidenav";
import {MatIconModule} from "@angular/material/icon";
import {MatDividerModule} from "@angular/material/divider";
import {MatListModule} from "@angular/material/list";
import {AsyncPipe, NgIf} from "@angular/common";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatButtonModule} from "@angular/material/button";
import {AnnoncesComponent} from "./annonces/annonces.component";


const routes: Routes = [
  { path:'',  component: DashbordComponent},
  { path:'annonces',  component: AnnoncesComponent},


];

@NgModule({
  imports: [RouterModule.forRoot(routes), MatSidenavModule, MatIconModule, MatDividerModule, MatListModule, AsyncPipe, MatToolbarModule, NgIf, MatButtonModule],
  declarations: [
    NavbarComponent,
  ],
  exports: [RouterModule, NavbarComponent]
})
export class AppRoutingModule { }
