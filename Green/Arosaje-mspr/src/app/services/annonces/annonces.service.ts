import { Injectable } from '@angular/core';
import {Observable, map, of, tap, throwError} from 'rxjs';
import {Annonces} from "../../models/annonces/annonces";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Plants} from "../../models/plants/plants";

@Injectable({
  providedIn: 'root'
})
export class AnnoncesService {

  private usersUrl='http://127.0.0.1:8000/api/posts';
  private httpOptions = new HttpHeaders({'Content-Type': 'application/json'});

  constructor(private http: HttpClient) { }

  getAnnonces(): Observable<Annonces[]>{
    return this.http.get<Annonces[]>('http://127.0.0.1:8000/api/posts', {headers : new HttpHeaders({
      'Accept': 'application/json'
    })
  });
  }


  getAnnonce(id : number): Observable<Annonces>{
    return this.http.get<Annonces>(this.usersUrl + '/' + id);
  }

  addAnnonce(annonces: Annonces): Observable<Annonces> {
    return this.http.post<Annonces>(this.usersUrl + '/add',annonces,{headers:this.httpOptions}).pipe(
      tap((newAnnonces : Annonces) => (`added user w/ id=${newAnnonces.id}`)),
    );
  }

  updateAnnonce(annonce: Annonces): Observable<Annonces>{
    return this.http.put<Annonces>(this.usersUrl + '/' + annonce.id,
      annonce, {headers : this.httpOptions});
  }

  deleteAnnonce(id:number): Observable<Annonces> {
    return this.http.delete<Annonces>(this.usersUrl + '/' + id, {
      headers : this.httpOptions
    });
  }
}
