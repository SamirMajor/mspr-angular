import { Injectable } from '@angular/core';
import {Observable, of, tap, throwError} from 'rxjs';
import {Users} from "../../models/users/users";
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  private usersUrl='http://127.0.0.1:8000/api/posts';
  private httpOptions = new HttpHeaders({'Content-Type': 'application/json'});

  constructor(private http: HttpClient) { }

  getUsers(): Observable<any>{
    console.log('serv get',this.http.get<any>('http://127.0.0.1:8000/api/posts'));
    return this.http.get<any>('http://127.0.0.1:8000/api/posts');
  }

  getUser(id : number): Observable<Users>{
    return this.http.get<Users>(this.usersUrl + '/' + id);
  }

  addUser(user: Users): Observable<Users> {
    return this.http.post<Users>(this.usersUrl + '/add',user,{headers:this.httpOptions}).pipe(
      tap((newUser : Users) => (`added user w/ id=${newUser.id}`)),
    );
  }

  updateUser(user: Users): Observable<Users>{
    return this.http.put<Users>(this.usersUrl + '/' + user.id,
      user, {headers : this.httpOptions});
  }

  deleteUser(id:number): Observable<Users> {
    return this.http.delete<Users>(this.usersUrl + '/' + id, {
      headers : this.httpOptions
    });
  }
}
