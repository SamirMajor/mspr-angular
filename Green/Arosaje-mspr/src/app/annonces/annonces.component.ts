import {AfterViewInit, Component, OnInit} from '@angular/core';
import {AnnoncesService} from "../services/annonces/annonces.service";
import {UsersService} from "../services/users/users.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-annonces',
  templateUrl: './annonces.component.html',
  styleUrls: ['./annonces.component.css']
})
export class AnnoncesComponent implements OnInit, AfterViewInit{
  annonces : any[];

  constructor(private annoncesServices : AnnoncesService, private router: Router) { }


  ngAfterViewInit(): void {
  }

  ngOnInit(): void {
    this.getAnnonces();
  }

  private getAnnonces():void{
    console.log('ldap comp');
    this.annoncesServices.getAnnonces().subscribe(
      data => {
        this.annonces = data;
        console.log(data );

      }
    );
  }
}
